# Corona Face Mask Detection

This app will detect in an image if there is a face of someone not wearing a protective health mask 
(widely used against Corona).

The app can be run in two ways:

* On a laptop using the laptop's camera. It will open a window with a live feed of the camera.
  Each face detected by the model will have a bounding box around it. 
  If the model detects that there are no protective mask,
  the box will become red and it will raise a sound alert.
  Otherwise, the box will be green.
* On a Raspberry Pi with a camera connected to it.
  Here it will make a sound alert when someone is not wearing a mask.

## Model

The classification is done using two consecutive models:

1. A `MTCNN` pretrained model is used to detect all faces on the image. 
Each faces is cropped and sent to the second model.

2. A `tiny Yolo` model trained on mouth detection will detect if there is a mouth on each face. 
If there are no mouth we can assume the person is wearing a mask. 
This second model was trained on a data set of celebrities with mouth coordinates.

## User guide

### Running it locally

To use this app:

1. Clone the repo. 
2. Then install the requirements using `pip install -r requirements.txt`.
3. Finally, run `python main.py`

### Running it on a Raspberry Pi

To use this app:

1. Clone the repo. 
2. Then install the requirements using `pip install -r requirements_pi.txt`.
3. Additionally you need to install tensorflow2.1 specific for your pi.
   Download the wheel file here: https://storage.googleapis.com/corona-vision/data_gcs/tensorflow-2.1.0-cp37-cp37m-linux_armv7l.whl
   Then you can install it with `pip install tensorflow-2.1.0-cp37-cp37m-linux_armv7l.whl`
3. Finally, run `python main_pi.py`

### Remarks

Note that in the `mouth_detected()` function from `helpers/helpers_yolo.py` you can change the `score_tresh` argument. 
If you increase it, it will less often predict a positive mouth. Hence, the model will raise less alerts. 
You can therefore calibrate so the sensitivity of the model fits your use case.
