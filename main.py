import cv2
import os
from mtcnn import MTCNN
from absl import app
from helpers.helpers import detect_faces_and_mouths, download_from_gcs
from helpers.helpers_yolo import initiate_yolo
from helpers.configs import BUCKET_NAME, BUCKET_DIR, DATA_PATH


def main(argv):
    # If no data, will download it from GCS
    if not os.path.isdir(os.path.join(DATA_PATH)):
        download_from_gcs(DATA_PATH, BUCKET_NAME, BUCKET_DIR)

    # Initiate models
    detector = MTCNN()
    args_flags, yolo = initiate_yolo()

    # Initiate webcam vision
    cap = cv2.VideoCapture(0)
    while True:
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Detect images
        detect_faces_and_mouths(frame, detector, yolo, args_flags)

        # Close window by pressing "q"
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    app.run(main)
