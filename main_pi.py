import time
import os
from mtcnn import MTCNN
from absl import app
from helpers.helpers import detect_faces_and_mouths_pi
from helpers.helpers_yolo import initiate_yolo
from helpers.configs import BUCKET_NAME, BUCKET_DIR, DATA_PATH

from picamera.array import PiRGBArray
from picamera import PiCamera


def main(argv):

    # Initiate models
    detector = MTCNN()
    args_flags, yolo = initiate_yolo()
    print('Initialization of models done')

    # Initiate webcam vision
    camera = PiCamera()
    raw_capture = PiRGBArray(camera)

    # allow the camera to warmup
    time.sleep(0.1)
    print('Camera ready')
    while True:
        # Capture frame-by-frame
        raw_capture.truncate()
        raw_capture.seek(0)
        camera.capture(raw_capture, format="bgr")
        frame = raw_capture.array

        # Detect images
        detect_faces_and_mouths_pi(frame, detector, yolo, args_flags)


if __name__ == '__main__':
    app.run(main)