"""Helpers linked to GCS, OpenCV and MTCNN."""
import time
import logging
import os
import cv2
from pygame import mixer
from google.cloud import storage
from helpers.helpers_yolo import mouth_detected
from helpers.configs import COLORS, DATA_PATH, SOUND_FILE


def download_from_gcs(data_path, bucket_name, bucket_dir):
    """Download data from GCS. Includes the YOLO model.
    Can take a while."""
    logging.info('Loading data from GCS (about 500MB of data).')
    os.mkdir(data_path)
    os.mkdir(os.path.join(data_path, 'models'))
    # storage_client = storage.Client.from_service_account_json(key_path)
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blobs = bucket.list_blobs(prefix=bucket_dir)  # Get list of files
    for blob in blobs:
        filename = blob.name
        if filename[-1] != '/':
            filename = filename.split('/')[-1]
            blob.download_to_filename(os.path.join(DATA_PATH, filename))


def draw_bounding_box(imgcv, results, make_noise=True):
    """Draws bounding boxes."""
    for res in results:
        box = res['face']
        if res['mouth_detected']:
            # Set box color to red
            box_color = COLORS['red']
            # Make noise
            if make_noise:
                # t1 = time.time()
                mixer.init()
                mixer.music.load(os.path.join(DATA_PATH, SOUND_FILE))
                mixer.music.play()
                # t2 = time.time()
                # logging.info('-' * 25)
                # logging.info('time for MUSIC: {}'.format(t2 - t1))
                # logging.info('-' * 25)
        else:
            box_color = COLORS['green']
        x_1, y_1, width, height = box['box']
        x_2 = x_1 + width
        y_2 = y_1 + height
        conf = box['confidence']
        if conf < 0.9:
            continue
        cv2.rectangle(imgcv, (x_1, y_1), (x_2, y_2), box_color, 6)
    return imgcv


def detect_faces_and_mouths(frame, detector, yolo, args_flags):
    """Takes an input frame, detects faces and then mouths and will return
    the frame with correpsonding bounding boxes and raise a sound alert if
    somebody is not wearing a mask in the image.
    """
    # Detect faces in the image
    t_1 = time.time()
    result_faces = detector.detect_faces(frame)
    t_2 = time.time()
    logging.info('time for MTCNN: {}'.format(t_2 - t_1))

    # Detect mouths
    results = []
    for res in result_faces:
        x_1, y_1, width, height = res['box']
        res_img = frame[y_1:y_1 + height, x_1:x_1 + width]
        mouth = mouth_detected(res_img, yolo, args_flags)
        results.append({'face': res, 'mouth_detected': mouth})

    # Draw boxes
    res_frame = draw_bounding_box(frame, results)

    # Show image
    cv2.imshow('frame', res_frame)


def detect_faces_and_mouths_pi(frame, detector, yolo, args_flags):
    """Takes an input frame, detects faces and then mouths and raise a sound alert if
    somebody is not wearing a mask in the image.
    """
    # Detect faces in the image
    t_1 = time.time()
    result_faces = detector.detect_faces(frame)
    t_2 = time.time()
    logging.info('time for MTCNN: {}'.format(t_2 - t_1))

    # Detect mouths
    results = []
    for res in result_faces:
        x_1, y_1, width, height = res['box']
        res_img = frame[y_1:y_1 + height, x_1:x_1 + width]
        mouth = mouth_detected(res_img, yolo, args_flags)
        if mouth:
            print('mouth detected')
            mixer.init()
            mixer.music.load(os.path.join(DATA_PATH, SOUND_FILE))
            mixer.music.play()
            time.sleep(0.5)
