"""Configs."""

DATA_PATH = 'data_gcs'
MODEL_VERSION = ''
SOUND_FILE = 'google_translate_voice.mp3'
BUCKET_NAME = 'corona-vision'
BUCKET_DIR = 'data_gcs'
MODEL_TF = 'yolov3_train_5.tf'

COLORS = {'red': (0, 0, 230), 'green': (0, 179, 0)}
