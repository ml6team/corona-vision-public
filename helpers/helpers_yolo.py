"""Helpers for the yolo mouth detection model."""
import logging
import os
import time
import tensorflow as tf
from absl import flags
from helpers.configs import DATA_PATH, MODEL_VERSION, MODEL_TF
from yolov3_tf2.models import YoloV3, YoloV3Tiny
from yolov3_tf2.dataset import transform_images


def initiate_yolo():
    """Loads and initiate the Yolo model."""
    # Initiate yolo args
    FLAGS = flags.FLAGS
    flags.DEFINE_string('classes', os.path.join(DATA_PATH, 'corona.names'),
                        'path to classes file')
    flags.DEFINE_string('weights',
                        os.path.join(DATA_PATH, MODEL_VERSION, MODEL_TF),
                        'path to weights file')
    flags.DEFINE_boolean('tiny', True, 'yolov3 or yolov3-tiny')
    flags.DEFINE_integer('size', 320, 'resize images to')
    flags.DEFINE_integer('num_classes', 1, 'number of classes in the model')

    # Lower threshold due to insufficient training
    FLAGS.yolo_iou_threshold = 0.01
    FLAGS.yolo_score_threshold = 0.01

    # Load model
    if FLAGS.tiny:
        yolo = YoloV3Tiny(classes=FLAGS.num_classes)
    else:
        yolo = YoloV3(classes=FLAGS.num_classes)

    yolo.load_weights(FLAGS.weights).expect_partial()
    logging.info('Yolo weights loaded from {}'.format(
        os.path.join(DATA_PATH, MODEL_VERSION, 'yolov3_train_3.tf')))
    return FLAGS, yolo


def mouth_detected(img_cv2, yolo, args_flags, score_tresh=0.4):
    """
    Launches the mouth detection.
    Args:
         img_cv2 (np.array): Image as returned by cv2, i.e. a numpy array.
            Note that it is already cropped using MTCNN.
         yolo (yolov3_tf2.model): The yolo model
         args_flags (absl.flag): Absl flags
         score_tresh (float): Threshold for the mouth prediction to be
            considered positive. It's important to calibrate it.
    Returns:
        (bool) Prediction of there being a mouth in the image.
    """
    img_raw = tf.convert_to_tensor(img_cv2)
    img = tf.expand_dims(img_raw, 0)

    img = tf.image.rgb_to_grayscale(img)
    img_black = tf.image.grayscale_to_rgb(img)

    img = transform_images(img_black, args_flags.size)

    t_1 = time.time()
    _, scores, _, _ = yolo(img)
    t_2 = time.time()
    logging.info('time for Yolo: {}'.format(t_2 - t_1))
    logging.info('scores[0][0]: {}'.format(scores[0][0]))
    logging.info('RESULT: {}'.format(scores[0][0] > score_tresh))

    return scores[0][0] > score_tresh
